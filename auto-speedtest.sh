#! /bin/bash

###############################################################################
# Title: Scheduled speedtest
# Author: mor88888888
# Descrition: Test your network and save the results.
# Dependencies:
#   - speedtest-cli (https://github.com/sivel/speedtest-cli)
###############################################################################

# Dir vars
current_day=$(date +%Y%m%d)
save_dir="/home/${USER}/.local/share/${current_day}_speedtest.csv"
log_dir="/home/${USER}/.local/share/${current_day}_speedtest.log"
# Time vars
sleep_time="1m" # Default
start_time=$(date +%s)
stop_time=$(date -d "+5 minutes" +%s) # Default

usage(){
	echo "Usage: ./auto-speedtest.sh [o save|l log|t time|s stop|h]"
	echo "-o --> Save directory output"
	echo "-l --> Save directory log"
	echo "-t --> Time between tests. See sleep man for input format"
	echo "-s --> Time limit. See date man for input format"
	echo "-h --> Show this help"
	echo "Example: ./auto-speedtest.sh -o ./test.csv -l ./test.log -t 1m -s \"5 minutes\""
  exit
}

bad_option(){
	echo "See -h for a summary of options."
	exit
}

# (POSIX variable) Reset in case getopts has been used previously in the shell.
OPTIND=1

# Check options given

while getopts "o:l:t:s:h" opt; do
	case "${opt}" in
		o) save_dir=$OPTARG;;
		l) log_dir=$OPTARG;;
		t) sleep_time=$OPTARG;;
    s) stop_time=$(date -d "+$OPTARG" +%s);;
		h) usage ;;
		*) bad_option ;;
esac
done

# A option is needed
#if (( $OPTIND == 1 )); then
#   bad_option
#fi

shift $((OPTIND-1))

# Init log
echo "#########################" > $log_dir

# Log timestamp
echo "[II] Started at $(date -d @$start_time)" >> $log_dir
echo "[II] It will finish at $(date -d @$stop_time)" >> $log_dir

# Check if today's csv file exist
if [[ ! -f "$save_dir" ]]; then
  touch $save_dir
else
  mv $save_dir "${save_dir}.old"
  touch $save_dir
  echo "[!!] Today's file already exist, tagged as old" >> $log_dir
fi

# Init csv file with headers
speedtest-cli --csv-header >> $save_dir
echo "[II] Wrote headers" >> $log_dir

# Info start the test
echo "The test has started, press CTRL+C to cancel"

# Main loop
while [[ $(date +%s) -le $stop_time ]]
do
  echo "[II] Testing at $(date +%T)" >> $log_dir
  speedtest-cli --csv >> $save_dir
  echo "[II] Sleeping $sleep_time" >> $log_dir
  sleep $sleep_time
done

# Finishing
echo "Finished"
echo "[II] Finished" >> $log_dir
echo "#########################" >> $log_dir

