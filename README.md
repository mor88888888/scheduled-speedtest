# Scheduled Speedtest

:construction: Beta version :construction:

Based on https://github.com/sivel/speedtest-cli, schedule a speed test in order to obtain a sample of your connection along the time.

Options:

```
/auto-speedtest.sh [o save|l log|t time|s stop|h]"
-o --> Save directory output"
-t --> Time between tests. See sleep man for input format"
-s --> Time limit. See date man for input format"
-h --> Show this help"
```

Usage example:

```
./auto-speedtest.sh -o ./test.csv -l ./test.log -t 1m -s "5 minutes"
```
